CREATE DATABASE  IF NOT EXISTS `materiais` 
USE `materiais`;

DROP TABLE IF EXISTS `en_aluno`;
CREATE TABLE `en_aluno` (
  `matricula` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`matricula`),
  UNIQUE KEY `matricula_UNIQUE` (`matricula`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `en_item`;
CREATE TABLE `en_item` (
  `codigo` bigint(20) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) NOT NULL,
  `quantidade` int(11) NOT NULL DEFAULT '0',
  `tipo` int(1) NOT NULL,
  `local` int(2) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`),
  KEY `fk_tipo_idx` (`tipo`),
  KEY `fk_local_idx` (`local`),
  CONSTRAINT `fk_local` FOREIGN KEY (`local`) REFERENCES `en_local` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tipo` FOREIGN KEY (`tipo`) REFERENCES `en_tipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `en_local`;
CREATE TABLE `en_local` (
  `id` int(2) NOT NULL,
  `nome` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `en_tipo`;
CREATE TABLE `en_tipo` (
  `id` int(1) NOT NULL,
  `nome` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `re_aluno_item`;
CREATE TABLE `re_aluno_item` (
  `matricula_aluno` bigint(20) NOT NULL,
  `codigo_item` bigint(20) NOT NULL,
  PRIMARY KEY (`matricula_aluno`,`codigo_item`),
  KEY `fk_item_re_aluno_item_idx` (`codigo_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
