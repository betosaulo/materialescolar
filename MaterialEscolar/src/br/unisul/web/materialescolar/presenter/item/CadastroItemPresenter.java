package br.unisul.web.materialescolar.presenter.item;

import java.util.List;

import br.unisul.web.materialescolar.api.dto.Item;
import br.unisul.web.materialescolar.api.dto.ItemColetivo;
import br.unisul.web.materialescolar.api.utils.ServiceException;
import br.unisul.web.materialescolar.app.api.AbstractPresenter;
import br.unisul.web.materialescolar.app.api.Application;
import br.unisul.web.materialescolar.app.api.View;
import br.unisul.web.materialescolar.presenter.RootPresenter;
import br.unisul.web.materialescolar.service.aluno.CadastroAlunoService;
import br.unisul.web.materialescolar.service.item.CadastroItemService;
import br.unisul.web.materialescolar.shared.enumerations.Tipo;
import br.unisul.web.materialescolar.shared.events.ItemEvent;
import br.unisul.web.materialescolar.shared.view.item.CadastroItemView;

import com.google.common.eventbus.Subscribe;


public class CadastroItemPresenter extends AbstractPresenter<CadastroItemView>{ 
	private CadastroItemView view;
	private CadastroItemService service;
	private CadastroAlunoService alunosService;
	private Tipo tipo;

	public CadastroItemPresenter(AbstractPresenter<? extends View> parent, Application app, Object parameter) {
		super(parent, app, parameter);
	}

	@Override
	public void init() {
		this.tipo = (Tipo) this.getParameter();
		this.service = new CadastroItemService();
		this.alunosService = new CadastroAlunoService();
		this.view = this.getView(CadastroItemView.class);
		this.view.render(this.tipo);
		((RootPresenter)this.parent).setContent(view);
		this.fillIn();
		this.fillInAlunos();
	}

	private void fillInAlunos() {
		try {
			this.view.fillInAlunos(this.alunosService.getAlunos());
		} catch (ServiceException e) {
			this.view.showErrorMessage();
		}
	}

	@SuppressWarnings("unchecked")
	private void fillIn() {
		try {
			if(tipo == Tipo.COLETIVO){
				this.view.fillInColetivos((List<ItemColetivo>) this.service.getItens(this.tipo));
			}else{
				this.view.fillInIndividual((List<Item>) this.service.getItens(this.tipo));
			}
		} catch (ServiceException e) {
			this.view.showErrorMessage();
		}
	}
	
	@Override
	protected void close() {
		
	}
	
	@Subscribe
	public void clickSaveItem(ItemEvent event){
		try {
			this.service.manterItem(event.getItem());
			this.fillIn();
		} catch (ServiceException e) {
			this.view.showErrorMessage();
		}
	}
	
}
