package br.unisul.web.materialescolar.presenter;

import br.unisul.web.materialescolar.app.api.AbstractPresenter;
import br.unisul.web.materialescolar.app.api.Application;
import br.unisul.web.materialescolar.app.api.View;
import br.unisul.web.materialescolar.presenter.aluno.CadastroAlunoPresenter;
import br.unisul.web.materialescolar.presenter.item.CadastroItemPresenter;
import br.unisul.web.materialescolar.shared.enumerations.Tipo;
import br.unisul.web.materialescolar.shared.events.MenuEvent;
import br.unisul.web.materialescolar.shared.view.RootView;

import com.google.common.eventbus.Subscribe;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;

public class RootPresenter extends AbstractPresenter<RootView>{

	private RootView view;
	private AbstractPresenter<? extends View> active;

	public RootPresenter(AbstractPresenter<?> parent, Application app) {
		super(parent,app);
	}

	@Override
	public void init() {
		this.view = this.getView(RootView.class);
		this.view.render();
		this.app.setView(this.view);
		if(this.active == null){
			this.active = new CadastroAlunoPresenter(this, app);
		}
	}
	
	private void logout(){
		this.active = null;
		VaadinSession.getCurrent().getSession().invalidate();
		VaadinSession.getCurrent().close();
		Page.getCurrent().reload();
	}
	
	public void setContent(View view) {
		this.view.setContent(view);
	}

	@Subscribe
	public void menuSelecionado(MenuEvent menuEvent) {
		if(this.active != null){
			this.active.finalize();
			this.active = null;
		}
		
		switch (menuEvent.getMenuSelecionado()) {
		case CADASTRO_ALUNO:
			this.active = new CadastroAlunoPresenter(this, this.app);
			break;
		case CADASTRO_ITEM_COLETIVO:
			this.active = new CadastroItemPresenter(this, this.app, Tipo.COLETIVO);
			break;
		case CADASTRO_ITEM_INDIVIDUAL:
			this.active = new CadastroItemPresenter(this, this.app, Tipo.INDIVIDUAL);
			break;
		case LOGOUT:
			this.logout();
			break;
		default:
			break;
		}
	}

	
	@Override
	protected void close() {
	}
	
}
