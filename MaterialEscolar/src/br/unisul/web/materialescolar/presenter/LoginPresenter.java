package br.unisul.web.materialescolar.presenter;

import br.unisul.web.materialescolar.api.dto.User;
import br.unisul.web.materialescolar.api.utils.UserData;
import br.unisul.web.materialescolar.api.utils.ValidateExeception;
import br.unisul.web.materialescolar.app.api.AbstractPresenter;
import br.unisul.web.materialescolar.app.api.Application;
import br.unisul.web.materialescolar.app.api.View;
import br.unisul.web.materialescolar.shared.events.LoginEvent;
import br.unisul.web.materialescolar.shared.view.LoginView;

import com.google.common.eventbus.Subscribe;

public class LoginPresenter extends AbstractPresenter<LoginView>{
	private LoginView view;

	public LoginPresenter(AbstractPresenter<View> parent, Application app) {
		super(parent, app);
	}
	@Override
	public void init() {
		this.view = this.getView(LoginView.class);
		this.view.render();
		this.app.setView(this.view);
	}
	
	@Subscribe
    public void userLoggedIn(LoginEvent event) {
		try {
			this.validateUser(event.getUser());
			UserData.set(event.getUser());
			new RootPresenter(this,this.app);
		} catch (ValidateExeception e) {
			this.view.showErroMessage();
		}
    }

	private void validateUser(User user) throws ValidateExeception {
		if(!("admin").equals(user.getUsuario()) || !("admin").equals(user.getSenha())){
			throw new ValidateExeception();		
		}
	}

	@Override
	protected void close() {
		
	}
}
