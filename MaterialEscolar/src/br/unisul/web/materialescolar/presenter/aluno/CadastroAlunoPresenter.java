package br.unisul.web.materialescolar.presenter.aluno;

import br.unisul.web.materialescolar.api.utils.ServiceException;
import br.unisul.web.materialescolar.app.api.AbstractPresenter;
import br.unisul.web.materialescolar.app.api.Application;
import br.unisul.web.materialescolar.app.api.View;
import br.unisul.web.materialescolar.presenter.RootPresenter;
import br.unisul.web.materialescolar.service.aluno.CadastroAlunoService;
import br.unisul.web.materialescolar.shared.events.AlunoEvent;
import br.unisul.web.materialescolar.shared.view.aluno.CadastroAlunoView;

import com.google.common.eventbus.Subscribe;

public class CadastroAlunoPresenter extends AbstractPresenter<CadastroAlunoView>{ 
	private CadastroAlunoView view;
	private CadastroAlunoService service;

	public CadastroAlunoPresenter(AbstractPresenter<? extends View> parent, Application app) {
		super(parent, app);
	}

	@Override
	public void init() {
		this.service = new CadastroAlunoService();
		this.view = this.getView(CadastroAlunoView.class);
		this.view.render();
		this.fillIn();
		((RootPresenter)this.parent).setContent(view);
	}

	private void fillIn() {
		try {
			this.view.fillIn(this.service.getAlunos());
		} catch (ServiceException e) {
			this.view.showErrorMessage();
		}
	}
	

	@Subscribe
	public void manterAlunos(AlunoEvent alunoEvent) {
		try {
			this.service.manterAluno(alunoEvent.getAluno());
			this.fillIn();
		} catch (ServiceException e) {
			this.view.showErrorMessage();
		}
	}
	
	@Override
	public void close() {
		
	}
}
