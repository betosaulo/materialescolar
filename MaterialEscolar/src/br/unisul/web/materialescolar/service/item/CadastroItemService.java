package br.unisul.web.materialescolar.service.item;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.unisul.web.materialescolar.api.dto.Aluno;
import br.unisul.web.materialescolar.api.dto.Item;
import br.unisul.web.materialescolar.api.dto.ItemColetivo;
import br.unisul.web.materialescolar.api.utils.ServiceException;
import br.unisul.web.materialescolar.service.util.AbstractInsert;
import br.unisul.web.materialescolar.service.util.AbstractQuery;
import br.unisul.web.materialescolar.service.util.AbstractSearch;
import br.unisul.web.materialescolar.shared.enumerations.Local;
import br.unisul.web.materialescolar.shared.enumerations.Tipo;

public class CadastroItemService {

	public List<? extends Item> getItens(Tipo tipo) throws ServiceException {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * ");
		sql.append(" FROM EN_ITEM ");
		sql.append(" WHERE tipo = ?");
					
		AbstractSearch<List<Item>> template = new AbstractSearch<List<Item>>() {

			@Override
			protected void prepare(PreparedStatement stmt) throws Exception {
				stmt.setLong(1, tipo.getCodigo());
				
			}

			@Override
			protected List<Item> processa(ResultSet rs) throws Exception {
				List<Item> lista = new ArrayList<Item>();
				
				while(rs.next()){
					Item item = tipo == Tipo.COLETIVO ? new ItemColetivo() : new Item();
					item.setCodigo(rs.getLong("codigo"));
					item.setDescricao(rs.getString("descricao"));
					item.setQuantidade(rs.getInt("quantidade"));
					if(tipo == Tipo.COLETIVO){
						((ItemColetivo)item).setLocal(Local.getByCodigo(rs.getInt("local")));
					}
					item.setLstAlunos(CadastroItemService.this.getLstAlunos(rs.getLong("codigo")));
					lista.add(item);
				}
				return lista;
			}
		};
		
		return template.execute(sql.toString());
	}
	
	protected List<Aluno> getLstAlunos(Long codigo) throws ServiceException {
		StringBuilder sql = new StringBuilder();
		
		sql.append(" SELECT A.* ");
		sql.append(" FROM RE_ALUNO_ITEM AS AI ");
		sql.append(" INNER JOIN EN_ALUNO AS A ON (AI.matricula_aluno = A.matricula) ");
		sql.append(" WHERE AI.codigo_item = ? ");
					
		AbstractSearch<List<Aluno>> template = new AbstractSearch<List<Aluno>>() {

			@Override
			protected void prepare(PreparedStatement stmt) throws Exception {
				stmt.setLong(1, codigo);
			}
			@Override
			protected List<Aluno> processa(ResultSet rs) throws Exception {
				List<Aluno> lista = new ArrayList<Aluno>();
				
				while(rs.next()){
					Aluno aluno = new Aluno();
					aluno.setMatricula(rs.getLong("matricula"));
					aluno.setNome(rs.getString("nome"));
					lista.add(aluno);
				}
				return lista;
			}
		};
		
		return template.execute(sql.toString());
	}

	public void manterItem(Item item) throws ServiceException{
		if(item.getCodigo() != null && item.getCodigo() > 0 ){
			this.editItem(item);
			this.addAlunos(item.getCodigo(),item.getLstAlunos());
		}else{
			Long codigo = this.addItem(item);
			this.addAlunos(codigo,item.getLstAlunos());
		}
	}

	private void addAlunos(Long codigo, List<Aluno> lstAlunos) throws ServiceException {
		
		this.removeAllAlunos(codigo);
		
		StringBuilder sql = new StringBuilder();
		sql.append(" INSERT INTO RE_ALUNO_ITEM ( matricula_aluno, codigo_item ) ");
		sql.append(" VALUES (?,?)");

		for (Aluno aluno : lstAlunos) {

			AbstractQuery query = new AbstractQuery() {
				
				@Override
				protected void prepare(PreparedStatement stmt) throws Exception {
					stmt.setLong(1, aluno.getMatricula());
					stmt.setLong(2, codigo);
				}
			};
			
			query.execute(sql.toString());
		}
	}

	private void removeAllAlunos(Long codigo) throws ServiceException {
		StringBuilder sql = new StringBuilder();
		sql.append(" DELETE FROM RE_ALUNO_ITEM ");
		sql.append(" WHERE codigo_item = ?");

		AbstractQuery query = new AbstractQuery() {
				
			@Override
			protected void prepare(PreparedStatement stmt) throws Exception {
				stmt.setLong(1, codigo);
			}
		};
			
		query.execute(sql.toString());
	}

	private Long addItem(final Item item) throws ServiceException{
		StringBuilder sql = new StringBuilder();

		if(item instanceof ItemColetivo){
			sql.append(" INSERT INTO EN_ITEM ( descricao, quantidade, tipo, local )  VALUES (?,?,?,?) ");
		}else{
			sql.append(" INSERT INTO EN_ITEM ( descricao, quantidade, tipo )  VALUES (?,?,?) ");
		}
		
		AbstractInsert<Long> template = new AbstractInsert<Long>() {
			@Override
			protected void prepare(PreparedStatement stmt) throws Exception {
				stmt.setString(1, item.getDescricao());
				stmt.setInt(2, item.getQuantidade());
				if(item instanceof ItemColetivo){
					stmt.setInt(3, Tipo.COLETIVO.getCodigo());
					stmt.setInt(4, ((ItemColetivo)item).getLocal().getCodigo());
				}else{
					stmt.setInt(3, Tipo.INDIVIDUAL.getCodigo());
				}
			}

			@Override
			protected Long processa(ResultSet rs) throws Exception {
				 if (rs.next()) {
					return rs.getLong(1);
		         }
				return null;
			}
		};
		
		return template.execute(sql.toString());
	}
	
	private boolean editItem(final Item item) throws ServiceException{
		
		StringBuilder sql = new StringBuilder();
		sql.append(" UPDATE EN_ITEM SET descricao = ?, quantidade = ?");
		if(item instanceof ItemColetivo){
			sql.append(" , local = ? ");
		}
		sql.append(" WHERE codigo = ? ");
		
		AbstractQuery template = new AbstractQuery() {
			@Override
			protected void prepare(PreparedStatement stmt) throws Exception {
				stmt.setString(1, item.getDescricao());
				stmt.setInt(2, item.getQuantidade());
				
				if(item instanceof ItemColetivo){
					stmt.setInt(3, ((ItemColetivo)item).getLocal().getCodigo());
					stmt.setLong(4, item.getCodigo());
				}else{
					stmt.setLong(3, item.getCodigo());
				}
			}
		};
		
		return template.execute(sql.toString());
	}

}
