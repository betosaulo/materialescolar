package br.unisul.web.materialescolar.service.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ConnectionFactory {

	ResourceBundle bundle = ResourceBundle.getBundle("conexao");
	
	private final String driver = bundle.getString("sql.driver");
	private final String host = bundle.getString("sql.host");
	private final String port = bundle.getString("sql.port");
	private final String base = bundle.getString("sql.base");
	private final String usuario = bundle.getString("sql.user");
	private final String senha = bundle.getString("sql.senha");
		 
	public Connection getConnection(){
		try {
			Class.forName("com.mysql.jdbc.Driver");  
			String connection =	"jdbc:" + this.driver + "://" + this.host + ":" + this.port + "/" + this.base;
			return DriverManager.getConnection(connection, this.usuario, this.senha);
		} catch (SQLException | ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
			
	
}