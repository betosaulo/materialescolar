package br.unisul.web.materialescolar.service.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.unisul.web.materialescolar.api.utils.ServiceException;

public abstract class AbstractInsert<T> {

	ConnectionFactory factory = new ConnectionFactory();
	private Connection connection = null;
	private PreparedStatement stmt = null;
	private ResultSet rs = null;
	
		
	public  T execute(String sql) throws ServiceException{
		try {
			this.connection = factory.getConnection();
			this.stmt = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			this.prepare(stmt);
			System.out.println(stmt);
			this.stmt.executeUpdate();
			this.rs = this.stmt.getGeneratedKeys();
			return this.processa(rs);
		} catch (SQLException e) {
			throw new ServiceException(e);
		} catch (Exception e) {
			throw new ServiceException(e);
		}finally{
			if(rs != null){
				try {
					rs.close();
					rs = null;
				} catch (SQLException e) {
					throw new ServiceException(e);
				}
			}
			if(stmt != null){
				try {
					stmt.close();
					stmt = null;
				} catch (SQLException e) {
					throw new ServiceException(e);
				}
			}
			if(connection != null){
				try {
					connection.close();
					connection = null;
				} catch (SQLException e) {
					throw new ServiceException(e);
				}
			}
		}
	}

	protected abstract void prepare(PreparedStatement stmt) throws Exception;
	
	protected abstract T processa(ResultSet rs) throws Exception;
	
}


//Statement stmt = db.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
//numero = stmt.executeUpdate();