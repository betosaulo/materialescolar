package br.unisul.web.materialescolar.service.aluno;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.unisul.web.materialescolar.api.dto.Aluno;
import br.unisul.web.materialescolar.api.utils.ServiceException;
import br.unisul.web.materialescolar.service.util.AbstractQuery;
import br.unisul.web.materialescolar.service.util.AbstractSearch;

public class CadastroAlunoService {


	public List<Aluno> getAlunos() throws ServiceException{
		
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * ");
		sql.append(" FROM EN_ALUNO ");
					
		AbstractSearch<List<Aluno>> template = new AbstractSearch<List<Aluno>>() {

			@Override
			protected void prepare(PreparedStatement stmt) throws Exception {
			}

			@Override
			protected List<Aluno> processa(ResultSet rs) throws Exception {
				List<Aluno> lista = new ArrayList<Aluno>();
				
							
				while(rs.next()){
					Aluno aluno = new Aluno();
					aluno.setNome(rs.getString("nome"));
					aluno.setMatricula(rs.getLong("matricula"));
					lista.add(aluno);
				}
				return lista;
			}
		};
		
		return template.execute(sql.toString());
	}

	public void manterAluno(Aluno aluno) throws ServiceException{
		if(aluno.getMatricula() != null && aluno.getMatricula() > 0 ){
			this.editAluno(aluno);
		}else{
			this.addAluno(aluno);
		}
	}
	
	private boolean addAluno(final Aluno aluno) throws ServiceException{
		StringBuilder sql = new StringBuilder();
		sql.append(" INSERT INTO EN_ALUNO ( nome ) ");
		sql.append(" VALUES (?)");
		
		AbstractQuery template = new AbstractQuery() {
			@Override
			protected void prepare(PreparedStatement stmt) throws Exception {
				stmt.setString(1, aluno.getNome());
			}
		};
		
		return template.execute(sql.toString());
	}
	
	private boolean editAluno(final Aluno aluno) throws ServiceException{
		
		StringBuilder sql = new StringBuilder();
		sql.append(" UPDATE EN_ALUNO SET ");
		sql.append("	nome = ? ");
		sql.append(" WHERE matricula = ? ");
		
		AbstractQuery template = new AbstractQuery() {
			@Override
			protected void prepare(PreparedStatement stmt) throws Exception {
				stmt.setString(1, aluno.getNome());
				stmt.setLong(2, aluno.getMatricula());
			}
		};
		
		return template.execute(sql.toString());
	}
	
}
