package br.unisul.web.materialescolar.app;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;

public class AppEventBus implements SubscriberExceptionHandler {
	
    private static final EventBus eventBus = new EventBus();

    public static void post(final Object event) {
    	AppEventBus.eventBus.post(event);
    }

    public static void register(final Object object) {
    	AppEventBus.eventBus.register(object);
    }

    public static void unregister(final Object object) {
    	AppEventBus.eventBus.unregister(object);
    }

	@Override
	public void handleException(Throwable exception, SubscriberExceptionContext context) {
		exception.printStackTrace();		
	}
}