package br.unisul.web.materialescolar.app.api;


public interface Application {
	
	public void setView(View view);
	
	public void setDetail(View view);

	public View getView(Class<? extends View> view);
	
}
