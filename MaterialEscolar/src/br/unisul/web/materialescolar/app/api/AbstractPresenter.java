package br.unisul.web.materialescolar.app.api;

import br.unisul.web.materialescolar.app.AppEventBus;

public abstract class AbstractPresenter<V extends View> {

	protected Application app;
	protected AbstractPresenter<? extends View> parent;
	private Object parameter;

	public AbstractPresenter(AbstractPresenter<? extends View> parent, Application app, Object parameter) {
		this.setParameter(parameter);
		AppEventBus.register(this);
		this.app = app;
		this.parent = parent;
		this.init();
	}

	public AbstractPresenter(AbstractPresenter<? extends View> parent, Application app) {
		AppEventBus.register(this);
		this.app = app;
		this.parent = parent;
		this.init();
	}
	
	public V getView(Class<V> viewClass) {
		@SuppressWarnings("unchecked")
		V view = (V) this.app.getView(viewClass);
		return view;
	}
	
	public void finalize(){
		AppEventBus.unregister(this);
		this.close();
	}
	

	public Object getParameter() {
		return parameter;
	}

	public void setParameter(Object parameter) {
		this.parameter = parameter;
	}

	protected abstract void close();
	
	protected abstract void init();
	
}
