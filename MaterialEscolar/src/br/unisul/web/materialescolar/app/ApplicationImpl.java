package br.unisul.web.materialescolar.app;

import br.unisul.web.materialescolar.api.utils.UserData;
import br.unisul.web.materialescolar.app.api.Application;
import br.unisul.web.materialescolar.app.api.View;
import br.unisul.web.materialescolar.app.injector.ViewFactory;
import br.unisul.web.materialescolar.presenter.LoginPresenter;
import br.unisul.web.materialescolar.presenter.RootPresenter;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class ApplicationImpl implements Application{

	private AppUI app;
	private Injector injector;
	private RootPresenter root;
	private LoginPresenter login;
	
	public ApplicationImpl(AppUI app) {
		this.app = app;
		AppEventBus.register(this);
		this.injector = Guice.createInjector(new ViewFactory());
		this.init();
	}
	
	public View getView(Class<? extends View> view){
		return this.injector.getInstance(view);
	}

	public void init() {
		if (UserData.isLoggedIn()) {
			if(this.root == null){
				this.root = new RootPresenter(null, this);				
			}
        } else {
        	if(this.login == null){
        		this.login = new LoginPresenter(null, this);
        	}
        }
	}
	
	@Override
	public void setView(View view) {
		this.app.setView(view);		
	}

	@Override
	public void setDetail(View view) {
		this.app.setDetail(view);
	}
}
