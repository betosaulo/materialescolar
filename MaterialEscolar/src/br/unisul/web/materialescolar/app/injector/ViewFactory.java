package br.unisul.web.materialescolar.app.injector;

import br.unisul.web.materialescolar.shared.view.LoginView;
import br.unisul.web.materialescolar.shared.view.RootView;
import br.unisul.web.materialescolar.shared.view.aluno.CadastroAlunoView;
import br.unisul.web.materialescolar.shared.view.item.CadastroItemView;
import br.unisul.web.materialescolar.view.LoginViewImpl;
import br.unisul.web.materialescolar.view.RootViewImpl;
import br.unisul.web.materialescolar.view.aluno.CadastroAlunosViewImpl;
import br.unisul.web.materialescolar.view.item.CadastroItemViewImpl;

import com.google.inject.AbstractModule;

public class ViewFactory extends AbstractModule {

	@Override
	protected void configure() {
		bind(LoginView.class).to(LoginViewImpl.class);
		bind(RootView.class).to(RootViewImpl.class);
		bind(CadastroAlunoView.class).to(CadastroAlunosViewImpl.class);
		bind(CadastroItemView.class).to(CadastroItemViewImpl.class);
		
	}

}