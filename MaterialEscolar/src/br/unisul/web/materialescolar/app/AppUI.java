package br.unisul.web.materialescolar.app;

import javax.servlet.annotation.WebServlet;

import br.unisul.web.materialescolar.app.api.View;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
@Theme("materialescolar")
@Title("Materiais escolares")
@PreserveOnRefresh
public class AppUI extends UI{

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = AppUI.class)
	public static class Servlet extends VaadinServlet {
	}

	private ApplicationImpl application;

	@Override
	protected void init(VaadinRequest request) {
		final VerticalLayout layout = new VerticalLayout();
		this.setContent(layout);
		layout.setSizeFull();
		layout.setMargin(new MarginInfo(true));
		if(this.application == null){
			this.application = new ApplicationImpl(this);
		}
		
	}

	public void setView(Component view) {
		this.setContent(view);
	}

	public void setDetail(View view) {
		if(view instanceof Window){
			this.addWindow((Window) view);			
		}else{
			Window subWindow = new Window("");
	        subWindow.setContent(view);
	        subWindow.center();
	        this.addWindow(subWindow);
		}
	}
}