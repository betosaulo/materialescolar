package br.unisul.web.materialescolar.view.item;

import java.util.ArrayList;
import java.util.List;

import br.unisul.web.materialescolar.api.dto.Aluno;
import br.unisul.web.materialescolar.api.dto.Item;
import br.unisul.web.materialescolar.api.dto.ItemColetivo;
import br.unisul.web.materialescolar.api.utils.LocalConverter;
import br.unisul.web.materialescolar.api.utils.MyTheme;
import br.unisul.web.materialescolar.shared.enumerations.Tipo;
import br.unisul.web.materialescolar.shared.view.item.CadastroItemView;
import br.unisul.web.materialescolar.view.item.detail.CadastroItemDetail;
import br.unisul.web.materialescolar.view.widget.DefaultPage;
import br.unisul.web.materialescolar.view.widget.NotificationUtil;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickEvent;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickListener;

public class CadastroItemViewImpl extends DefaultPage implements CadastroItemView{

	private static final long serialVersionUID = 1L;
	private BeanItemContainer<?> container;
	private Grid grid;
	private List<Aluno> lstAlunos;
	private Tipo tipo;

	public CadastroItemViewImpl() {
		this.init();
	}

	private void init() {
	}
	
	@Override
	public void showErrorMessage() {
		NotificationUtil.showErrorMessage("Erro ao acessar a base de dados!");
	}

	@Override
	public void render(Tipo tipo) {
		this.tipo = tipo;
		this.renderTitulo();
		this.renderButtonBar();
	    this.renderContentList();
	}
	
	private void renderTitulo() {
		this.setTitle("Cadastro de itens " + (tipo == Tipo.COLETIVO ? "coletivos": "individuais"));		
	}

	@Override
	public void fillInAlunos(List<Aluno> lstAlunos) {
		this.lstAlunos = lstAlunos;
	}
	
	@Override
	public void fillInIndividual(List<Item> lstItensColetivos){
		this.container = new BeanItemContainer<Item>(Item.class, lstItensColetivos);
		this.grid.setContainerDataSource(this.initGridProperties());
	}
	@Override
	public void fillInColetivos(List<ItemColetivo> lstItensColetivos){
		this.container = new BeanItemContainer<ItemColetivo>(ItemColetivo.class, lstItensColetivos);
		this.grid.setContainerDataSource(this.initGridProperties());
	}

	private void renderButtonBar() {
		HorizontalLayout buttonBar = new HorizontalLayout();
		buttonBar.setWidth("100%");
		this.addComponent(buttonBar);
		this.renderBotaoAdicionar(buttonBar);
	}

	private void renderBotaoAdicionar(HorizontalLayout buttonBar) {
		Button button = new Button("Adicionar item");
		button.addClickListener(new OnclickAddItem());
		button.setStyleName(MyTheme.BUTTON_PRIMARY);
		buttonBar.addComponent(button);
		buttonBar.setComponentAlignment(button, Alignment.BOTTOM_RIGHT);
	}
	
	private void renderContentList() {
		VerticalLayout content = new VerticalLayout();
		content.setSizeFull();
		this.addComponent(content);
		this.setComponentAlignment(content, Alignment.TOP_LEFT);
		this.setExpandRatio(content, 1.f);
		this.renderGrid(content);
	}

	private void renderGrid(VerticalLayout content) {

		if(this.tipo == Tipo.COLETIVO){
			this.container = new BeanItemContainer<ItemColetivo>(ItemColetivo.class, new ArrayList<ItemColetivo>());
		}else{
			this.container = new BeanItemContainer<Item>(Item.class, new ArrayList<Item>());
		}
		
		this.grid = new Grid(this.initGridProperties());
		this.grid.setSizeFull();
		this.grid.setSelectionMode(SelectionMode.NONE);
		this.grid.setColumnReorderingAllowed(true);
		this.grid.getColumn("codigo").setHeaderCaption("Codigo").setWidthUndefined();
		this.grid.getColumn("descricao").setHeaderCaption("Descri��o").setExpandRatio(1);
		this.grid.getColumn("quantidade").setHeaderCaption("Quantidade").setWidthUndefined();
		this.grid.getColumn("editar").setWidth(90).setRenderer(new ButtonRenderer(new OnClickEditar()));
		this.grid.removeColumn("lstAlunos");
		
		if(this.tipo == Tipo.COLETIVO){
			this.grid.getColumn("local").setHeaderCaption("Local").setWidth(100).setConverter(new LocalConverter());
			this.grid.setColumnOrder("codigo", "descricao", "quantidade","local","editar");
		}else{
			this.grid.setColumnOrder("codigo", "descricao", "quantidade","editar");
		}

		content.addComponent(this.grid);
		content.setComponentAlignment(this.grid, Alignment.MIDDLE_CENTER);
	}
	
	@SuppressWarnings("serial")
	private GeneratedPropertyContainer initGridProperties(){
		GeneratedPropertyContainer gpc = new GeneratedPropertyContainer(this.container);
		gpc.addGeneratedProperty("editar", new PropertyValueGenerator<String>() {

			@Override
			public String getValue(com.vaadin.data.Item item, Object itemId, Object propertyId) {
				return "Editar";
			}

			@Override
			public Class<String> getType() {
				return String.class;
			}
		});
		return gpc;
	}
	
	@SuppressWarnings("serial")
	private class OnClickEditar implements RendererClickListener{
		@Override
		public void click(RendererClickEvent event) {
			getUI().addWindow(new CadastroItemDetail((Item)event.getItemId(), lstAlunos, tipo));
		}
	}
	
	@SuppressWarnings("serial")
	private class OnclickAddItem implements Button.ClickListener{
		@Override
		public void buttonClick(ClickEvent event) {
			getUI().addWindow(new CadastroItemDetail(tipo == Tipo.COLETIVO ? new ItemColetivo() : new Item(), lstAlunos, tipo));
		}
	}
	
}
