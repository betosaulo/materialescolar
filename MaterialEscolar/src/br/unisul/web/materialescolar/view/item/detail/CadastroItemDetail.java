package br.unisul.web.materialescolar.view.item.detail;

import java.util.ArrayList;
import java.util.List;

import br.unisul.web.materialescolar.api.dto.Aluno;
import br.unisul.web.materialescolar.api.dto.Item;
import br.unisul.web.materialescolar.api.dto.ItemColetivo;
import br.unisul.web.materialescolar.api.utils.MyTheme;
import br.unisul.web.materialescolar.app.AppEventBus;
import br.unisul.web.materialescolar.shared.enumerations.Local;
import br.unisul.web.materialescolar.shared.enumerations.Tipo;
import br.unisul.web.materialescolar.shared.events.ItemEvent;
import br.unisul.web.materialescolar.view.widget.NotificationUtil;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.MultiSelectionModel;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class CadastroItemDetail extends Window {

	private TextField codigo;
	private TextField descricao;
	private Item item;
	private TextField quantidade;
	private ComboBox local;
	private List<Aluno> lstAlunos;
	private Grid grid;
	private Tipo tipo;
	
	public CadastroItemDetail(Item item, List<Aluno> lstAlunos, Tipo tipo) {
		super("Cadastro de items");
		this.tipo = tipo;
		this.lstAlunos = lstAlunos;
		this.init();
		this.render();
		this.fillIn(item);
	}

	public void fillIn(Item item) {
		this.item = tipo == Tipo.COLETIVO ? new ItemColetivo() : new Item() ;
		
		this.item.setCodigo(item.getCodigo());
		this.item.setDescricao(item.getDescricao());
		this.item.setQuantidade(item.getQuantidade());
		this.item.setLstAlunos(item.getLstAlunos());

		if(item instanceof ItemColetivo){
			((ItemColetivo)this.item).setLocal(((ItemColetivo)item).getLocal());	
			
			if(((ItemColetivo)this.item) != null){
				this.local.setValue(((ItemColetivo)this.item).getLocal());
			}
		}

		this.descricao.setValue(this.item.getDescricao());		
		
		if(this.item.getQuantidade() != null){
			this.quantidade.setValue(this.item.getQuantidade().toString());	
		}

		if(this.item.getCodigo() != null){
			this.codigo.setValue(this.item.getCodigo().toString());	
		}
		
		
		if(this.item.getLstAlunos() != null){
			this.fillAlunos(this.item.getLstAlunos());
		}
	}

	private void fillAlunos(List<Aluno> lstAlunos) {
		MultiSelectionModel selection =  (MultiSelectionModel) grid.getSelectionModel();
		selection.setSelected(lstAlunos);
	}

	private void render() {
		VerticalLayout layout = new VerticalLayout();
		layout.setHeightUndefined();
		layout.setWidth("100%");
		layout.setMargin(true);
		layout.setSpacing(true);
		this.center();
		this.setContent(layout);
		this.renderCampos(layout);
		this.renderGrid(layout);
		this.renderBottonBar(layout);
	}
	
	private void renderGrid(VerticalLayout content) {

	    BeanItemContainer<Aluno> container = new BeanItemContainer<Aluno>(Aluno.class, this.lstAlunos);
		
		this.grid = new Grid(container);
		this.grid.setSizeFull();
		this.grid.setCaption(tipo == Tipo.COLETIVO ? "Alunos que entregaram o material coletivo" : "Alunos que n�o possuem o material individual");
		this.grid.setSelectionMode(SelectionMode.MULTI);
		this.grid.setColumnReorderingAllowed(true);
		this.grid.getColumn("matricula").setHeaderCaption("Matricula").setWidthUndefined();
		this.grid.getColumn("nome").setHeaderCaption("Nome").setExpandRatio(1);
		this.grid.setColumnOrder("matricula", "nome");

		content.addComponent(this.grid);
		content.setComponentAlignment(this.grid, Alignment.MIDDLE_CENTER);
	}
	
	
	private void renderBottonBar(VerticalLayout layout) {
		HorizontalLayout buttonBar = new HorizontalLayout();
		buttonBar.setWidth("100%");
		layout.addComponent(buttonBar);
		this.renderBotaoAdicionar(buttonBar);
	}

	private void renderBotaoAdicionar(HorizontalLayout buttonBar) {
		Button button = new Button("Salvar");
		button.addClickListener(new OnclickSave());
		button.setStyleName(MyTheme.BUTTON_PRIMARY);
		buttonBar.addComponent(button);
		buttonBar.setComponentAlignment(button, Alignment.BOTTOM_RIGHT);
	}

	private void renderCampos(VerticalLayout layout) {
		HorizontalLayout campos = new HorizontalLayout();
		campos.setSpacing(true);
		campos.setMargin(false);
		layout.addComponent(campos);
		this.renderMatricula(campos);
		this.renderNome(campos);
		this.renderQuantidade(campos);
		this.renderLocal(campos);
	}

	private void renderLocal(HorizontalLayout campos) {
		this.local = new ComboBox("Local");
		this.local.setItemCaptionMode(ItemCaptionMode.EXPLICIT_DEFAULTS_ID);
		for (Local localEnum : Local.values()) {
			this.local.addItem(localEnum);
			this.local.setItemCaption(localEnum, localEnum.getNome());
		}
		this.local.addValidator(new BeanValidator(ItemColetivo.class, "local"));
		this.local.setConverter(Local.class);
		this.local.setNullSelectionAllowed(false);
		this.local.setNewItemsAllowed(false);
		this.local.setTextInputAllowed(false);
		this.local.setHeightUndefined();
		this.local.setWidthUndefined();
		this.local.setValidationVisible(false);
		campos.addComponent(this.local);
		this.local.setVisible( this.tipo == Tipo.COLETIVO);
	}

	private void renderQuantidade(HorizontalLayout campos) {
		this.quantidade = new TextField();
		this.quantidade.addValidator(new BeanValidator(ItemColetivo.class, "quantidade"));
		this.quantidade.setHeightUndefined();
		this.quantidade.setWidthUndefined();
		this.quantidade.setCaption("Quantidade");
		this.quantidade.setConverter(Integer.class);
		this.quantidade.setNullRepresentation("0");
		this.quantidade.setNullSettingAllowed(true);
		this.quantidade.setValidationVisible(false);
		campos.addComponent(this.quantidade);
	}

	private void renderNome(HorizontalLayout campos) {
		this.descricao = new TextField();
		this.descricao.addValidator(new BeanValidator(ItemColetivo.class, "descricao"));
		this.descricao.setHeightUndefined();
		this.descricao.setWidth("300px");
		this.descricao.setCaption("Descri��o");
		this.descricao.setNullRepresentation("");
		this.descricao.setNullSettingAllowed(true);
		this.descricao.setValidationVisible(false);
		campos.addComponent(this.descricao);
	}

	private void renderMatricula(HorizontalLayout campos) {
		this.codigo = new TextField();
		this.codigo.setHeightUndefined();
		this.codigo.setWidth("80px");
		this.codigo.setEnabled(false);
		this.codigo.setCaption("Codigo");
		this.codigo.setNullRepresentation("");
		this.codigo.setNullSettingAllowed(true);
		campos.addComponent(this.codigo);
	}

	private void init() {
		this.setClosable(true);
		this.setResizable(false);
		this.setModal(true);
		this.setHeightUndefined();
		this.setWidthUndefined();
	}
	
	private class OnclickSave implements Button.ClickListener{
		@Override
		public void buttonClick(ClickEvent event) {
			try {
				AppEventBus.post(new ItemEvent(CadastroItemDetail.this.getItem()));
				CadastroItemDetail.this.close();
			} catch (InvalidValueException e) {
				NotificationUtil.showValidationMesage(e.getCauses());
			}
		}
	}

	public void update() {		
		this.descricao.setValidationVisible(true);
		this.descricao.validate();
		this.quantidade.setValidationVisible(true);
		this.quantidade.validate();
		
		if(item instanceof ItemColetivo){
			this.local.setValidationVisible(true);
			this.local.validate();
			((ItemColetivo)this.item).setLocal((Local)this.local.getConvertedValue());			
		}
		
		this.item.setDescricao(this.descricao.getValue());
		this.item.setQuantidade((Integer)this.quantidade.getConvertedValue());
		
		
		List<Aluno> lstAlunosSelecionados = new ArrayList<>();
		for (Object select : this.grid.getSelectedRows()) {
			if(select instanceof Aluno){
				lstAlunosSelecionados.add((Aluno)select);
			}
		}
		this.item.setLstAlunos(lstAlunosSelecionados);
	}

	public Item getItem() {
		this.update();
		return this.item;
	} 
	
}
