package br.unisul.web.materialescolar.view.aluno;

import java.util.ArrayList;
import java.util.List;

import br.unisul.web.materialescolar.api.dto.Aluno;
import br.unisul.web.materialescolar.api.utils.MyTheme;
import br.unisul.web.materialescolar.shared.view.aluno.CadastroAlunoView;
import br.unisul.web.materialescolar.view.aluno.detail.CadastroAlunoDetail;
import br.unisul.web.materialescolar.view.widget.DefaultPage;
import br.unisul.web.materialescolar.view.widget.NotificationUtil;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickEvent;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickListener;

public class CadastroAlunosViewImpl extends DefaultPage implements CadastroAlunoView{

	private static final long serialVersionUID = 1L;
	private Grid grid;
	private BeanItemContainer<Aluno> container;

	public CadastroAlunosViewImpl() {
		this.setTitle("Cadastro alunos");
		this.init();
	}

	@Override
	public void fillIn(List<Aluno> lstAlunos) {
		this.container = new BeanItemContainer<Aluno>(Aluno.class, lstAlunos);
		this.grid.setContainerDataSource(this.initGridProperties());
	}
	
	private void init() {
	}

	@Override
	public void render() {
		this.renderButtonBar();
	    this.renderContentList();
	}

	private void renderButtonBar() {
		HorizontalLayout buttonBar = new HorizontalLayout();
		buttonBar.setWidth("100%");
		this.addComponent(buttonBar);
		this.renderBotaoAdicionar(buttonBar);
	}

	private void renderBotaoAdicionar(HorizontalLayout buttonBar) {
		Button button = new Button("Adicionar aluno");
		button.addClickListener(new OnclickAddAluno());
		button.setStyleName(MyTheme.BUTTON_PRIMARY);
		buttonBar.addComponent(button);
		buttonBar.setComponentAlignment(button, Alignment.BOTTOM_RIGHT);
	}

	private void renderContentList() {
		VerticalLayout content = new VerticalLayout();
		content.setSizeFull();
		this.addComponent(content);
		this.setComponentAlignment(content, Alignment.TOP_LEFT);
		this.setExpandRatio(content, 1.f);
		this.renderGrid(content);
	}

	private void renderGrid(VerticalLayout content) {

		this.container = new BeanItemContainer<Aluno>(Aluno.class, new ArrayList<Aluno>());
		
		this.grid = new Grid(this.initGridProperties());
		this.grid.setSizeFull();
		this.grid.setSelectionMode(SelectionMode.SINGLE);
		this.grid.setColumnReorderingAllowed(true);
		this.grid.getColumn("matricula").setHeaderCaption("Matricula").setWidthUndefined();
		this.grid.getColumn("nome").setHeaderCaption("Nome").setExpandRatio(1);
		this.grid.getColumn("editar").setWidth(90).setRenderer(new ButtonRenderer(new OnClickEditar()));
		this.grid.setColumnOrder("matricula", "nome", "editar");

		content.addComponent(this.grid);
		content.setComponentAlignment(this.grid, Alignment.MIDDLE_CENTER);
	}
	
	@SuppressWarnings("serial")
	private GeneratedPropertyContainer initGridProperties(){
		GeneratedPropertyContainer gpc = new GeneratedPropertyContainer(this.container);
		gpc.addGeneratedProperty("editar", new PropertyValueGenerator<String>() {

			@Override
			public String getValue(Item item, Object itemId, Object propertyId) {
				return "Editar";
			}

			@Override
			public Class<String> getType() {
				return String.class;
			}
		});
		return gpc;
	}
	
	
	private class OnClickEditar implements RendererClickListener{
		private static final long serialVersionUID = 1L;

		@Override
		public void click(RendererClickEvent event) {
			getUI().addWindow(new CadastroAlunoDetail((Aluno) event.getItemId()));
		}
		
	}
	
	private class OnclickAddAluno implements Button.ClickListener{
		private static final long serialVersionUID = 1L;

		@Override
		public void buttonClick(ClickEvent event) {
			getUI().addWindow(new CadastroAlunoDetail());
		}
	}

	@Override
	public void showErrorMessage() {
		NotificationUtil.showErrorMessage("Erro ao acessar a base de dados!");
	}
}
