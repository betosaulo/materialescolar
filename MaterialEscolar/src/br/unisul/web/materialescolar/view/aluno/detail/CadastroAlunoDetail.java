package br.unisul.web.materialescolar.view.aluno.detail;

import br.unisul.web.materialescolar.api.dto.Aluno;
import br.unisul.web.materialescolar.api.utils.MyTheme;
import br.unisul.web.materialescolar.app.AppEventBus;
import br.unisul.web.materialescolar.shared.events.AlunoEvent;
import br.unisul.web.materialescolar.view.widget.NotificationUtil;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class CadastroAlunoDetail extends Window {

	private TextField matricula;
	private TextField nome;
	private Aluno aluno;

	public CadastroAlunoDetail() {
		this(new Aluno());
	}
	
	public CadastroAlunoDetail(Aluno aluno) {
		super("Cadastro de alunos");
		this.init();
		this.render();
		this.fillIn(aluno);
	}

	public void fillIn(final Aluno aluno) {
		this.aluno = new Aluno();
		this.aluno.setNome(aluno.getNome());
		this.aluno.setMatricula(aluno.getMatricula());
		
		if(this.aluno.getNome() != null){
			this.nome.setValue(this.aluno.getNome());						
		}
		if(this.aluno.getMatricula() != null){
			this.matricula.setValue(this.aluno.getMatricula().toString());			
		}
	}

	private void render() {
		VerticalLayout layout = new VerticalLayout();
		layout.setHeightUndefined();
		layout.setWidth("100%");
		layout.setMargin(true);
		layout.setSpacing(true);
		this.center();
		this.setContent(layout);
		this.renderCampos(layout);
		this.renderBottonBar(layout);
	}

	private void renderBottonBar(VerticalLayout layout) {
		HorizontalLayout buttonBar = new HorizontalLayout();
		buttonBar.setWidth("100%");
		layout.addComponent(buttonBar);
		this.renderBotaoAdicionar(buttonBar);
	}

	private void renderBotaoAdicionar(HorizontalLayout buttonBar) {
		Button button = new Button("Salvar");
		button.addClickListener(new OnclickAddAluno());
		button.setStyleName(MyTheme.BUTTON_PRIMARY);
		buttonBar.addComponent(button);
		buttonBar.setComponentAlignment(button, Alignment.BOTTOM_RIGHT);
	}

	private void renderCampos(VerticalLayout layout) {
		HorizontalLayout campos = new HorizontalLayout();
		campos.setSpacing(true);
		campos.setMargin(false);
		layout.addComponent(campos);
		this.renderMatricula(campos);
		this.renderNome(campos);
	}

	private void renderNome(HorizontalLayout campos) {
		this.nome = new TextField();
		this.nome.addValidator(new BeanValidator(Aluno.class, "nome"));
		this.nome.setHeightUndefined();
		this.nome.setWidth("300px");
		this.nome.setCaption("Nome");
		this.nome.setNullRepresentation("");
		this.nome.setValidationVisible(false);
		campos.addComponent(this.nome);
	}

	private void renderMatricula(HorizontalLayout campos) {
		this.matricula = new TextField();
		this.matricula.setHeightUndefined();
		this.matricula.setWidth("80px");
		this.matricula.setEnabled(false);
		this.matricula.setCaption("Matricula");
		campos.addComponent(this.matricula);
	}

	private void init() {
		this.setClosable(true);
		this.setResizable(false);
		this.setModal(true);
		this.setHeightUndefined();
		this.setWidthUndefined();
	}
	
	private class OnclickAddAluno implements Button.ClickListener{
		@Override
		public void buttonClick(ClickEvent event) {
			try {
				AppEventBus.post(new AlunoEvent(CadastroAlunoDetail.this.getAluno()));
				CadastroAlunoDetail.this.close();
			} catch (InvalidValueException e) {
				NotificationUtil.showValidationMesage(e.getCauses());
			}
		}
	}

	public void update() {
		this.aluno.setNome(this.nome.getValue());
		this.nome.setValidationVisible(true);
		this.nome.validate();
	}

	public Aluno getAluno() {
		this.update();
		return this.aluno;
	} 
	
}
