package br.unisul.web.materialescolar.view.widget;

import br.unisul.web.materialescolar.api.utils.MyTheme;
import br.unisul.web.materialescolar.api.utils.UserData;
import br.unisul.web.materialescolar.shared.enumerations.Menu;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class MenuWidget extends CustomComponent{
	
	private Label nomeUsuario;
	private VerticalLayout rootContainer;
	private VerticalLayout containerMenu;

	public MenuWidget() {
		this.init();
		this.render();
		this.fillIn();
	}

	private void fillIn() {
		this.containerMenu.removeAllComponents();
		for (Menu menu : Menu.values()) {
			MenuItem item = new MenuItem(menu);
			this.containerMenu.addComponent(item);
		}
	}

	private void init() {
		this.setHeight("100%");
		this.setWidth("160px");
		this.setStyleName(MyTheme.MENU_VIEW);
		this.initRootContainer();
	}

	private void initRootContainer() {
		this.rootContainer = new VerticalLayout();
		this.setCompositionRoot(this.rootContainer);
		this.rootContainer.setSpacing(true);
		this.rootContainer.setMargin(false);
	}

	private void render() {
		this.renderUser();
		this.renderContainerMenu();
	}

	private void renderContainerMenu() {
		this.containerMenu = new VerticalLayout();
		this.containerMenu.setMargin(false);
		this.containerMenu.setSpacing(true);
		this.rootContainer.addComponent(this.containerMenu);
	}

	private void renderUser() {
		VerticalLayout containerUser = new VerticalLayout();
		containerUser.setMargin(false);
		this.rootContainer.addComponent(containerUser);
		
		this.renderBoasVindas(containerUser);
		this.renderUserName(containerUser);
	}

	private void renderBoasVindas(VerticalLayout userContainer) {
		Label boasVindas = new Label();
		boasVindas.setSizeFull();
		boasVindas.setValue("Bem vindo(a)");
		boasVindas.setStyleName(MyTheme.LABEL_LIGHT);
		userContainer.addComponent(boasVindas);
	}

	private void renderUserName(VerticalLayout userContainer) {
		this.nomeUsuario = new Label();
		this.nomeUsuario.setValue(UserData.get().getUsuario());
		this.nomeUsuario.setSizeFull();
		userContainer.addComponent(this.nomeUsuario);
	}
	
	
}
