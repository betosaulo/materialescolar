package br.unisul.web.materialescolar.view.widget;

import br.unisul.web.materialescolar.api.utils.MyTheme;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public abstract class DefaultPage extends VerticalLayout{
	
	private static final long serialVersionUID = 1L;
	private Label tituloLabel;
	
	public DefaultPage() {
		this.setStyleName(MyTheme.PAGE_ANIMATION);
		this.setSpacing(true);
		this.setMargin(true);
		this.renderTitle();
	}

	private void renderTitle() {
		this.tituloLabel = new Label();
		this.tituloLabel.setValue("");
		this.tituloLabel.setWidth("100%");
		this.tituloLabel.setStyleName(MyTheme.TITULO);
		this.addComponent(this.tituloLabel);
		this.setComponentAlignment(this.tituloLabel, Alignment.TOP_LEFT);
	}
	
	
	protected void setTitle(String title){
		this.tituloLabel.setValue(title);
	}
}
