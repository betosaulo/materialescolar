package br.unisul.web.materialescolar.view.widget;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

public class NotificationUtil {

	
	public NotificationUtil() {
		
	}
	
	public static void showErrorMessage(String message) {
		Notification notification = new Notification("Aten��o!",message,Type.ERROR_MESSAGE);
		notification.setIcon(FontAwesome.EXCLAMATION_CIRCLE);
		notification.setHtmlContentAllowed(true);
		notification.setPosition(Position.TOP_CENTER);
		notification.setDelayMsec(5000);
		notification.show(Page.getCurrent());	
	}
	
	public static void showValidationMesage(InvalidValueException[] causes){
		Notification notification = new Notification("Aten��o!",getMessage(causes),Type.WARNING_MESSAGE);
		notification.setIcon(FontAwesome.INFO_CIRCLE);
		notification.setHtmlContentAllowed(true);
		notification.setPosition(Position.TOP_CENTER);
		notification.setDelayMsec(5000);
		notification.show(Page.getCurrent());	
	}
	
	private static String getMessage(InvalidValueException[] causes) {
		String message = "<ul>";
		for (InvalidValueException invalidValueException : causes) {
			message += "<li>" + invalidValueException.getMessage() + "</li>";
		}
		
		return "</ul>" + message;
	}
}
