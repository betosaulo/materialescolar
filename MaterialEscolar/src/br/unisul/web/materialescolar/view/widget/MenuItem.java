package br.unisul.web.materialescolar.view.widget;

import br.unisul.web.materialescolar.api.utils.MyTheme;
import br.unisul.web.materialescolar.app.AppEventBus;
import br.unisul.web.materialescolar.shared.enumerations.Menu;
import br.unisul.web.materialescolar.shared.events.MenuEvent;

import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class MenuItem extends CustomComponent {

	private Menu menu;
	private VerticalLayout rootContainer;

	public MenuItem(Menu menu) {
		this.menu = menu;
		this.init();
		this.render();
	}

	private void render() {
		this.rendeIcone();
		this.renderLabel();
	}

	private void rendeIcone() {
		Image ico = new Image();
		ico.setSource(new ThemeResource(this.menu.getIcone()));
		this.rootContainer.addComponent(ico);
		this.rootContainer.setComponentAlignment(ico, Alignment.MIDDLE_CENTER);
	}

	private void renderLabel() {
		Label nomeMenu = new Label();
		nomeMenu.setValue(menu.getNome());
		nomeMenu.setSizeFull();
		nomeMenu.setStyleName(MyTheme.MENU_LABEL);
		this.rootContainer.addComponent(nomeMenu);
		this.rootContainer.setComponentAlignment(nomeMenu, Alignment.MIDDLE_CENTER);
	}

	private void init() {
		this.setSizeFull();
		this.setHeight("90");
		this.initRootContainer();
	}

	private void initRootContainer() {
		this.rootContainer = new VerticalLayout();
		this.rootContainer.setStyleName(MyTheme.MENU_ITEM);
		this.rootContainer.setSizeFull();
		this.rootContainer.setSpacing(false);
		this.rootContainer.setMargin(true);
		this.rootContainer.addLayoutClickListener(new OnClickMenu());
		this.setCompositionRoot(this.rootContainer);
	}
	
	private class OnClickMenu implements LayoutClickListener{

		@Override
		public void layoutClick(LayoutClickEvent event) {
			AppEventBus.post(new MenuEvent(MenuItem.this.menu));
		}
	}
	
	
}
