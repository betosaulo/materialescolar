package br.unisul.web.materialescolar.view;

import br.unisul.web.materialescolar.app.api.View;
import br.unisul.web.materialescolar.shared.view.RootView;
import br.unisul.web.materialescolar.view.widget.MenuWidget;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

public class RootViewImpl extends HorizontalLayout implements RootView{
	private static final long serialVersionUID = 1L;
	private Grid grid;
	private MenuWidget menu;
	private VerticalLayout content;
	
	public RootViewImpl() {
		this.init();
	}

	private void init() {
		this.setMargin(false);
		this.setSizeFull();
	}

	@Override
	public void render() {
		this.renderenuWidget();
		this.renderContent();
	}
	
	private void renderContent() {
		this.content = new VerticalLayout();
		this.content.setSizeFull();
		this.addComponent(content);
		this.setComponentAlignment(this.content, Alignment.TOP_LEFT);
		this.setExpandRatio(content, 1.f);
	}

	private void renderenuWidget() {
		this.menu = new MenuWidget();
		this.addComponent(menu);
		this.setComponentAlignment(menu, Alignment.TOP_LEFT);
	}
	
	@Override
	public void setContent(View view) {
		this.content.removeAllComponents();
		view.setSizeFull();
		this.content.addComponent(view);
	}
}
