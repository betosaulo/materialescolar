package br.unisul.web.materialescolar.view;

import br.unisul.web.materialescolar.api.dto.User;
import br.unisul.web.materialescolar.api.utils.MyTheme;
import br.unisul.web.materialescolar.app.AppEventBus;
import br.unisul.web.materialescolar.shared.events.LoginEvent;
import br.unisul.web.materialescolar.shared.view.LoginView;

import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class LoginViewImpl extends CustomComponent implements LoginView{
	
	private static final long serialVersionUID = 1L;
	private VerticalLayout containerLogin;
	private TextField usuario;
	private PasswordField senha;
	private Button btnLogin;
	private VerticalLayout layout;

	
	public LoginViewImpl() {
		this.initialConfig();
	}

	@Override
	public void render() {
		this.renderContainerLogin();		
	}
	
	private void initialConfig() {
		this.setSizeFull();
		this.layout = new VerticalLayout();
		this.layout.setMargin(true);
		this.setCompositionRoot(layout);
	}

	public User getUser() {
		User user = new User();
		user.setUsuario(this.usuario.getValue());
		user.setSenha(this.senha.getValue());
		return user;
	}
	
	
	private void renderButton() {
		this.btnLogin = new Button("Login");
		this.btnLogin.setStyleName(MyTheme.BUTTON_PRIMARY);
		this.btnLogin.addClickListener(new OnClickListener());
		this.containerLogin.addComponent(this.btnLogin);
		this.containerLogin.setComponentAlignment(btnLogin, Alignment.BOTTOM_RIGHT);
	}

	private void renderContainerLogin() {
		
		this.containerLogin = new VerticalLayout();
		this.containerLogin.setSpacing(true);
		this.containerLogin.setMargin(true);
		this.containerLogin.setSizeUndefined();
		this.containerLogin.setStyleName(MyTheme.LOGIN_VIEW);
		
		
		this.renderUsuario(this.containerLogin);
		this.renderSenha(this.containerLogin);
		this.renderButton();
		
		layout.addComponent(this.containerLogin);
		layout.setComponentAlignment(containerLogin, Alignment.MIDDLE_CENTER);
		layout.setSizeFull();
	}

	private class OnClickListener implements ClickListener{
		private static final long serialVersionUID = 1L;

		@Override
		public void buttonClick(ClickEvent event) {
			AppEventBus.post(new LoginEvent(LoginViewImpl.this.getUser()));
		}
	}
	
	
	private void renderUsuario(VerticalLayout parent) {
		this.usuario = new TextField("Usu�rio");
		this.usuario.setIcon(FontAwesome.USER);
		this.usuario.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
		parent.addComponent(usuario);
	}
	
	private void renderSenha(VerticalLayout parent) {
		this.senha = new PasswordField("Senha");
		this.senha.setIcon(FontAwesome.KEY);
		this.senha.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
		this.senha.addShortcutListener(new OnPressEnter());
		parent.addComponent(senha);
	}

	
	@SuppressWarnings("serial")
	private class OnPressEnter extends ShortcutListener{

		public OnPressEnter() {
			super("loguin", ShortcutAction.KeyCode.ENTER, null);
		}

			@Override
		public void handleAction(Object sender, Object target) {
			AppEventBus.post(new LoginEvent(LoginViewImpl.this.getUser()));
		}
		
	}
	
	@Override
	public void showErroMessage() {
		Notification notification = new Notification("Aten��o!","Login ou senha inv�lidos",Type.WARNING_MESSAGE);
		notification.setIcon(FontAwesome.INFO);
		notification.setPosition(Position.TOP_CENTER);
		notification.setDelayMsec(2000);
		notification.show(Page.getCurrent());
	}
	
}
