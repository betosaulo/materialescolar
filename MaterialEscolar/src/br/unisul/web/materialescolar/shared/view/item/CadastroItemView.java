package br.unisul.web.materialescolar.shared.view.item;

import java.util.List;

import br.unisul.web.materialescolar.api.dto.Aluno;
import br.unisul.web.materialescolar.api.dto.Item;
import br.unisul.web.materialescolar.api.dto.ItemColetivo;
import br.unisul.web.materialescolar.app.api.View;
import br.unisul.web.materialescolar.shared.enumerations.Tipo;

public interface CadastroItemView extends View {

	void showErrorMessage();

	void fillInIndividual(List<Item> lstItens);

	void fillInColetivos(List<ItemColetivo> lstItens);
	
	void fillInAlunos(List<Aluno> lstAlunos);

	void render(Tipo tipo);


}
