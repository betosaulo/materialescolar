package br.unisul.web.materialescolar.shared.view.aluno;

import java.util.List;

import br.unisul.web.materialescolar.api.dto.Aluno;
import br.unisul.web.materialescolar.app.api.View;

public interface CadastroAlunoView extends View {

	void fillIn(List<Aluno> modelAlunos);

	void showErrorMessage();

	void render();
	
}
