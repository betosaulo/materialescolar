package br.unisul.web.materialescolar.shared.view;

import br.unisul.web.materialescolar.app.api.View;

public interface LoginView extends View{

	void showErroMessage();

	void render();

}
