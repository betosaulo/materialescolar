package br.unisul.web.materialescolar.shared.view;

import br.unisul.web.materialescolar.app.api.View;

public interface RootView extends View {

	void setContent(View view);

	void render();

}
