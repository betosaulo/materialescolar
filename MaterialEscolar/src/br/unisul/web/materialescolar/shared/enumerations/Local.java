package br.unisul.web.materialescolar.shared.enumerations;

public enum Local {
	
	PRATELEIRA(1,"Prateleira"),
	ARMARIO(2,"Armario"),
	MESA(3,"Mesa"),
	CAIXA(4,"Caixa")
	;
	
	private int codigo;
	private String nome;

	private Local(int codigo, String nome){
		this.setCodigo(codigo);
		this.setNome(nome);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCodigo() {
		return codigo;
	}
	
	public static Local getByCodigo(int codigo){
		for (Local local : Local.values()) {
			if(local.getCodigo() == codigo){
				return local;
			}
		}
		return null;
	}
	public static Local getByName(String nome){
		for (Local local : Local.values()) {
			if(local.getNome().equals(nome)){
				return local;
			}
		}
		return null;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	@Override
    public String toString() {
        return nome;
    }
	
}
