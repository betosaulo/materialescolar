package br.unisul.web.materialescolar.shared.enumerations;


public enum Menu{

	CADASTRO_ALUNO(1,"Cadastro aluno","img/add-aluno.png"),
	CADASTRO_ITEM_COLETIVO(2,"Cadastro item coletivo","img/add-coletivo.png"),
	CADASTRO_ITEM_INDIVIDUAL(3,"Cadastro item individual","img/add-individual.png"),
	LOGOUT(4,"Logout","img/logout.png"),
	;
	
	private String nome;
	private int id;
	private String icone;


	private Menu(int id,String nome, String icone) {
		this.id = id;
		this.nome = nome;
		this.icone = icone;
	}

	public int getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getIcone() {
		return icone;
	}
	public static Menu getById(int id){
		for (Menu menu : Menu.values()) {
			if(menu.getId() == id){
				return menu;
			}
		}
		return null;
	}

}