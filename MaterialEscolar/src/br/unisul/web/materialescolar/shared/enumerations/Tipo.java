package br.unisul.web.materialescolar.shared.enumerations;

public enum Tipo {

	INDIVIDUAL(1),
	COLETIVO(2);
	
	private int codigo;
	/**
	 * @return the codigo
	 */
	public int getCodigo() {
		return codigo;
	}

	private Tipo(int codigo){
		this.codigo = codigo;
	}
	
	public static Tipo getByCodigo(int codigo){
		for (Tipo tipo : Tipo.values()) {
			if(tipo.getCodigo() == codigo){
				return tipo;
			}
		}
		return null;
	}
		
}
