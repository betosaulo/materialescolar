package br.unisul.web.materialescolar.shared.events;

import br.unisul.web.materialescolar.api.dto.Item;

public class ItemEvent {

	private Item item;

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}
	
	public ItemEvent(Item item) {
		this.item = item;
	}

}
