package br.unisul.web.materialescolar.shared.events;

import br.unisul.web.materialescolar.shared.enumerations.Menu;

public class MenuEvent {
	
	private Menu menuSelecionado;

	public MenuEvent(Menu menuSelecionado) {
		this.menuSelecionado = menuSelecionado;
	}

	public Menu getMenuSelecionado() {
		return menuSelecionado;
	}

	public void setMenuSelecionado(Menu menuSelecionado) {
		this.menuSelecionado = menuSelecionado;
	}

}
