package br.unisul.web.materialescolar.shared.events;

import br.unisul.web.materialescolar.api.dto.Aluno;

public class AlunoEvent {

	private Aluno aluno;
	
	/**
	 * @return the aluno
	 */
	public Aluno getAluno() {
		return aluno;
	}
	/**
	 * @param aluno the aluno to set
	 */
	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	
	public AlunoEvent(Aluno aluno) {
		this.aluno = aluno;
	}
}
