package br.unisul.web.materialescolar.shared.events;

import br.unisul.web.materialescolar.api.dto.User;

public class LoginEvent {
    private User user;

    public LoginEvent(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}