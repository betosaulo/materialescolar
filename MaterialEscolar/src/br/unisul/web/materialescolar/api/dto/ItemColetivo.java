package br.unisul.web.materialescolar.api.dto;

import javax.validation.constraints.NotNull;

import br.unisul.web.materialescolar.shared.enumerations.Local;

public class ItemColetivo extends Item{
	
	@NotNull(message="O campo local � obrigat�rio!")
	private Local local;

	/**
	 * @return the local
	 */
	public Local getLocal() {
		return local;
	}

	/**
	 * @param local the local to set
	 */
	public void setLocal(Local local) {
		this.local = local;
	}
}
