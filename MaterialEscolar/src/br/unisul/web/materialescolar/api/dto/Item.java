package br.unisul.web.materialescolar.api.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.unisul.web.materialescolar.shared.enumerations.Tipo;

public class Item {
	private Long codigo;
	
	@NotNull(message="O campo nome � obrigat�rio!")
	@Size(min=3,max=50,message="O campo descri��o deve conter no minimo 3 e no m�ximo 50 caracteres!")
	private String descricao;
	
	@NotNull(message="O campo quantidade � obrigat�rio!")
	@Min(value=0, message = "O campo quantidade � obrigat�rio!" )
	private Integer quantidade;
	
	@NotEmpty(message="� necess�rio que pelo menos um aluno seja selecionado!")
	private List<Aluno> lstAlunos;
	
	public Item() {
		lstAlunos = new ArrayList<Aluno>();
	}
	
	/**
	 * @return the codigo
	 */
	public Long getCodigo() {
		return codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * @return the quantidade
	 */
	public Integer getQuantidade() {
		return quantidade;
	}
	/**
	 * @param quantidade the quantidade to set
	 */
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	/**
	 * @return the lstAlunos
	 */
	public List<Aluno> getLstAlunos() {
		return lstAlunos;
	}
	/**
	 * @param lstAlunos the lstAlunos to set
	 */
	public void setLstAlunos(List<Aluno> lstAlunos) {
		this.lstAlunos = lstAlunos;
	}
	
}
