package br.unisul.web.materialescolar.api.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;



public class Aluno {

	
	private Long matricula;

	@NotNull(message="O campo nome � obrigat�rio!")
	@Size(min=3,max=50,message="O campo nome de conter no minimo de 3 e m�ximo de 50 caracteres!")
	private String nome;
	
	/**
	 * @return the matricula
	 */
	public Long getMatricula() {
		return matricula;
	}
	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(Long matricula) {
		this.matricula = matricula;
	}
	/**
	 * 
	 * @param matricula
	 */
	public void setMatricula(String matricula) {
		this.matricula = Long.parseLong(matricula);
	}
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((matricula == null) ? 0 : matricula.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		return true;
	}
	
}
