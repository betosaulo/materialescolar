package br.unisul.web.materialescolar.api.utils;

import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.ui.renderers.ClickableRenderer;


@SuppressWarnings("serial")
public class CheckBoxRenderer extends ClickableRenderer<Boolean> {

	public CheckBoxRenderer(Class<Boolean> presentationType) {
		super(presentationType);
		addClickListener(new RendererClickListener() {
			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			public void click(RendererClickEvent event) {
				System.err.println("Click event from client!");
				if (event.getColumn().isEditable() && getParentGrid().isEditorEnabled()) {
					Object itemId = event.getItemId();
					Object propertyId = event.getPropertyId();

					Container.Indexed containerDataSource = getParentGrid().getContainerDataSource();
					Property itemProperty = containerDataSource.getItem(itemId).getItemProperty(propertyId);

					itemProperty.setValue(!Boolean.TRUE.equals(itemProperty.getValue()));

					getParentGrid().editItem(itemId);
				}
			}
		});
	}

}
