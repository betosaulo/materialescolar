package br.unisul.web.materialescolar.api.utils;

public class ServiceException extends Exception {
	
	public ServiceException(Exception e) {
		super(e);
	}

	private static final long serialVersionUID = 1L;

}