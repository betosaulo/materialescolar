package br.unisul.web.materialescolar.api.utils;

import com.vaadin.ui.themes.ValoTheme;

public class MyTheme extends ValoTheme {

	public static final String LOGIN_VIEW = "login_view";
	public static final String MENU_VIEW = "menu_view";
	public static final String MENU_ITEM = "menu_item";
	public static final String MENU_LABEL = "menu_label";
	public static final String PAGE_ANIMATION = "page_animation";
	public static final String TITULO = "titulo";
	
}
