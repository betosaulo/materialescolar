package br.unisul.web.materialescolar.api.utils;

import br.unisul.web.materialescolar.api.dto.User;

import com.vaadin.server.VaadinSession;

public class UserData {

    private static final String KEY = "currentser";
	private static User user;

    public static void set(User user) {
    	UserData.user = user;
        VaadinSession.getCurrent().setAttribute(KEY, user != null ? user.hashCode() : null);
    }

    public static User get() {
    	if(VaadinSession.getCurrent().getAttribute(KEY) != null){
    		return user;
    	}
        return null;
    }

    public static boolean isLoggedIn() {
        return VaadinSession.getCurrent().getAttribute(KEY) != null;
    }
}