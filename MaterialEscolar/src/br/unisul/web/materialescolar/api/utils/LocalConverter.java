package br.unisul.web.materialescolar.api.utils;

import java.util.Locale;

import br.unisul.web.materialescolar.shared.enumerations.Local;

import com.vaadin.data.util.converter.Converter;

@SuppressWarnings("serial")
public class LocalConverter implements Converter<String, Local>{

	@Override
	public Local convertToModel(String value,	Class<? extends Local> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		return Local.getByName(value);
	}

	@Override
	public String convertToPresentation(Local value,	Class<? extends String> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		
		return value.getNome();
	}

	@Override
	public Class<Local> getModelType() {
		return Local.class;
	}

	@Override
	public Class<String> getPresentationType() {
		return String.class;
	}

}
